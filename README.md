# poetry

Install and configure poetry

## Role Variables

* `installer`
  * Type: String
  * Usages: URL to installer download

```
poetry:
  installer: https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polkhan.poetry

## License

MIT
